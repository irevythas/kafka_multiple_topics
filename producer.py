from json import dumps
import time
from kafka import KafkaProducer

producer = KafkaProducer(bootstrap_servers=['localhost:9092'],
                         value_serializer=lambda x:
                         dumps(x).encode('utf-8'))

topic1 = 'test1'
topic2 = 'test2'
topic3 = 'test3'

def producing_json():
    print('Start producing json')

    # read the file
    for e in range(10):
#        data  = {'number-1' : str(e)}
#        data2 = {'number-2' : str(1000 + e)}
#        data3 = {'number-3' : str(2*1000 + e)}

        data = { 'test1' : str(e),
                 'test2' : '',
                 'test3' : ''
                }
        print(data)
        producer.send(topic1, value=data)

        data = { 'test1' : '',
                 'test2' : str(100 + e),
                 'test3' : ''
                }

        print(data)
        producer.send(topic2, value=data)

        data = { 'test1' : '',
                 'test2' : '',
                 'test3' : str(1000 + e)
                }

        print(data)
        producer.send(topic3, value=data)
        # To reduce CPU usage create sleep time of 1 sec  
        time.sleep(1)
    print('Done!')

if __name__ == '__main__':
    producing_json()         
