from kafka import KafkaConsumer
from json import loads
import multiprocessing

def consume_data_proc(topic_name):
    print(topic_name)
    consumer = KafkaConsumer(
    topic_name,
     bootstrap_servers=['localhost:9092'],
     auto_offset_reset='earliest',
     enable_auto_commit=True,
     group_id='my-group',
     value_deserializer=lambda x: loads(x.decode('utf-8')))

    for message in consumer:
        message = message.value
        print(message)

if __name__ == "__main__":
    topics=['test1', 'test2', 'test3']
    jobs = []
    for topic_name in topics:
        p = multiprocessing.Process(target=consume_data_proc, args=(topic_name,))
        jobs.append(p)
        p.start()
