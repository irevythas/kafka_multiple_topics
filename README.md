This project has a producer and a consumer. The consumer, consume from kafka-topics and dive records into Cassandra Table.

- bin/zookeeper-server-start.sh config/zookeeper.properties

- bin/kafka-server-start.sh config/server.properties

- bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic numtest

- bin/kafka-topics.sh --describe --zookeeper localhost:2181 --topic  numtest

- bin/kafka-topics.sh --list --zookeeper localhost:2181
